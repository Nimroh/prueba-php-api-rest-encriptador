/*
Autor: Oscar Bolaños
Fecha: 2022-06-29
Última Modificación: 2022-06-29
Proceso:
	Encripta un $sTexto segun una $nLLave moviendo la posicion de los $sCaracteres
Paramentros:
	$sTexto: String ; texto a encriptar
	$nLLave: Integer ; numero entero positivo referente a la llave de encriptación
Retorna:
	String ; Json ; Texto encriptado segun la llave
*/
function encriptar($sTexto, $nLLave){
	
	//PARAMETROS INTERNOS
	$sCaracteres 	= "ABCDEFGHIJKLMNOPQRSTUVWXYZ"; //String de caracteres a comparar para encriptación

	$aCaracteres 	= str_split($sCaracteres);	//divide los caracteres en un array
	$aTexto 		= str_split($sTexto);	//divide el texto a encriptar en un array
	$nContador 		= count($aCaracteres);	//cuenta el numero de Caracteres y lo mantiene en memoria
	$nLLave			= (int)$nLLave;
	
	//VALIDACIONES
	if($sTexto == ""){ //valida que el texto no este vacio
		die('El texto no puede ser vacio');
	}
	
	if(!is_int($nLLave) OR $nLLave < 1){ //valida que la llave sea entero positivo mayor o igual a 1
		die('La llave debe ser un numero entero positivo');
	}
	
	foreach($aTexto as $x => $v){ //recorre el texto letra por letra a encriptar
		$nMover = array_search($aTexto[$x], $aCaracteres); //calcula la posición del indice del array buscando caracter por caracter de la palabra
		if($nMover !== false){ //valida que los caracteres de $sTexto se encuentren dentro del conjunto $sCaracteres
			$nMover	+= $nLLave;
		}
		else{	
			die('Ocurrio un problema; El texto a encriptar tiene uno o varios caracteres que no se encuentran en el conjunto de caracteres predeterminado.');
		}
		if($nMover > $nContador){	//verifica si la nueva posición sobrepasa el numero de caracteres
			$nMover = $nMover % $nContador;	//asigna la nueva posición segun el residuo de la división
		}
		$aResultado[] = $aCaracteres[$nMover];	//genera el array resultado
	}
	return json_encode($aResultado); //retorna y parsea a un json
}

if($_GET){
	echo encriptar($_GET['texto'], $_GET['llave']);
}
