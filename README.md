# Prueba PHP Api Rest Encriptador

Encripta un texto segun una llave

# Test Local

http://localhost/pruebaApiRestEncriptar/?texto=CASA&llave=5

retorna
["H","F","X","F"]

http://localhost/pruebaApiRestEncriptar/?texto=TU&llave=10

retorna
["D","E"]

# Test Online

https://oasistemas.com/pruebaphp/?texto=CASA&llave=5

https://oasistemas.com/pruebaphp/?texto=TU&llave=10

# UML


| apirest encriptar |
| ------ |
| $sTexto: String |
| $nLLave: Integer |
| + encriptar |
